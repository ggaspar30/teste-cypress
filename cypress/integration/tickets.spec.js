describe("Tickets", ()=>{
    beforeEach(()=> cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));// direciona para abrir a url desejada
//Testes = it
//it.only executa somente aquele teste
it("fills all the text input fields", ()=>{
    const firstName = "Gustavo";
    const lastName ="Gaspar";

    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type("gustavo.p.gaspar@hotmail.com");
    cy.get("#requests").type("TESTE");
    cy.get("#signature").type(`${firstName} ${lastName}`);
});// preenchando campos com textos

it("select two tickets", ()=>{

    cy.get("#ticket-quantity").select("2");

});// selecionando valor no select

it("select 'vip' ticket type", ()=>{
    cy.get("#vip").check();
});// selecionando checkbox

it("selects 'social media' checkbox", ()=>{

    cy.get("#social-media").check();

});


it("alertas de email invalido", ()=>{

    cy.get("#email")
    .as("email")
    .type("gustavo.p.gaspar-hotmail.com");

    cy.get("#email.invalid").should("exist");

    cy.get("@email")
        .clear()
        .type("gustavo.p.gaspar@hotmail.com");

    cy.get("#email.invalid").should("not.exist");

});


it("has 'TICKETBOS' header's heading", ()=>{
    cy.get("header h1").should("contain", "TICKETBOX");

});

it("preenchendo o form e limpando ele", ()=>{

    const firstName = "Gustavo";
    const lastName ="Gaspar";
    const fullName = `${firstName} ${lastName}`;

    cy.get("#first-name").type(firstName);
    cy.get("#last-name").type(lastName);
    cy.get("#email").type("gustavo.p.gaspar@hotmail.com");
    cy.get("#requests").type("TESTE Gustavo");
    cy.get("#signature").type(`${firstName} ${lastName}`);
    cy.get("#ticket-quantity").select("2");
    cy.get("#vip").check();
    cy.get("#social-media").check();

    // cy.get(".agreement p").should((
    //     "contain",
    //     `I, ${fullName}, wish to buy 2 VIP tickets.`
    // ));

    

    cy.get("#agree").click(); // metodo click e check ambos usados para checkbox
    cy.get("#signature").type(fullName);

    cy.get("button[type='submit']")
    .as("submitButton")
    .should("not.be.disabled");

    cy.get("button[type='reset']").click();

    cy.get("@submitButton").should("be.disabled");

});
        

it("fills mandatory fields using support command", ()=>{

    const customer = new Object()
    customer.firstName = "Lucas",
    customer.lastName = "Silva",
    customer.email = "lucasSilva@gmail.com"

    cy.fillMandatoryFields(customer);

    cy.get("button[type='submit']")
    .as("submitButton")
    .should("not.be.disabled");

    cy.get("#agree").uncheck();

    cy.get("@submitButton").should("be.disabled");

});


});
